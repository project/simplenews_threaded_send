<?php

/**
 * @file class.Thread.php
 * Allow execution of asynchronous scripts.
 */


class Thread {

  public $_socket = FALSE;
  
  // Whether or not to keep the response from the server
  private $keep_response = FALSE;
  
  // Response from the server, only used if $keep_response is TRUE
  private $response = '';
  

  /**
   * Constructor
   */
  public function __construct($script = NULL, $variables = array(), $connection_timeout = 30, $execution_timeout = 86400, $keep_response = FALSE) {
    $this->keep_response = $keep_response;
    
    if (!empty($script)) {
      $this->start($script, $variables, $connection_timeout, $execution_timeout);
    }
  }
  
  /**
   * Save the response to this thread
   */
  public function keepResponse() {
    $this->keep_response = TRUE;
  }
  
  /**
   * Do NOT save the response to this thread
   */
  public function ignoreResponse() {
    $this->keep_response = FALSE;
  }
  
  /**
   * Get the response to this thread
   */
  public function getResponse() {
    return $this->response;
  }


  /**
   * Check to see if the connection is active.
   */
  public function active() {
    return $this->_socket !== FALSE;
  }


  /**
   * Start executing a script.
   */
  public function start($script, $variables = array(), $connection_timeout = 30, $execution_timeout = 86400) {
    if (!$this->active()) {
      // Clear the response from any previous calls. Only matters if $this->keep_response is true
      $this->response = '';

      $variables = (array)$variables;

      $error_number = '';
      $error_string = '';

      set_time_limit(0);

      $script_parsed = parse_url($script);
      $host = $script_parsed['host'];
      $url  = $script_parsed['path'];
      $port = isset($script_parsed['port']) ? $script_parsed['port'] : 80;

      $this->_socket = fsockopen($host, $port, $error_number, $error_string, $connection_timeout);
      if ($this->_socket === FALSE) {
        error_log("PHPMailer Threading class: $error_string ($error_number)");
        return FALSE;
      }

      stream_set_blocking($this->_socket, FALSE);
      stream_set_timeout($this->_socket, $execution_timeout);

      $output  = "POST $url HTTP/1.1\r\n";
      $output .= "Host: $host\r\n";
      $output .= "Accept: text/html\r\n";
      $output .= "User-Agent: MajorityLeader\r\n";
      $output .= "Content-type: application/x-www-form-urlencoded\r\n";

      if (!empty($variables)) {
        $post_data = http_build_query($variables, '', '&');
        $output .= 'Content-length: ' . strlen($post_data) . "\r\n";
      } else {
        $output .= "Content-length: 0\r\n";
      }

      $output .= "Connection: Close\r\n\r\n";

      if (!empty($post_data)) {
        $output .= "$post_data\r\n\r\n";
      }
      
      fwrite($this->_socket, $output);
    }
  }


  /**
   * Poll a thread to see if the script is still executing.
   *
   * @return mixed
   *   FALSE if HTTP disconnect (EOF), or a string (could be empty string) if still connected
   */
  public function poll() {

    if ($this->_socket === FALSE) {
      return FALSE;
    }

    if (feof($this->_socket)) {
      fclose($this->_socket);
      $this->_socket = FALSE;
      return FALSE;
    }

    $line = fread($this->_socket, 1000);
    
    // Only save the returned value if keeping the response from the server
    if($this->keep_response) {
      $this->response .= $line;
    }

    return $line;
  }
}