By default, Simplenews assumes that only one process is sending mail at a time. This module adds the ability to send mail across several processes at once.

Obviously, this requires the Simplenews module, but it also uses Multi SMTP. For each SMTP server that is configured, a mail sending thread is created.

Note that sending email from multiple threads will result in the bug explained in this bug in the Simplenews issue queue: #361071: Multiple emails revisited - 50% too many <http://drupal.org/node/361071>. I'm working on including a workaround for this bug in this module, but in the meantime, use the patch in comment #19 on that issue.

This module doesn't add any admin options or interfaces; it just works in the background.